﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTGDI.eTS.WebApi.API;
using PTGDI.eTS.WebApi.Models;

namespace PTGDI.eTS.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WAReceiverController : ControllerBase
    {
        private static readonly WAApi api = new WAApi("https://eu135.chat-api.com/instance139685/", "0xbu4x60y2nu2ktt");
        private static readonly string welcomeMessage = "Bot's menu: \n" +
                                                        "1. chatid - Get chatid\n";

        [HttpPost]
        public async Task<string> Post(Answer data)
        {
            foreach (var message in data.Messages)
            {
                if (message.FromMe)
                    continue;

                switch (message.Body.Split()[0].ToLower())
                {
                    case "chatid":
                        return await api.SendMessage(message.ChatId, $"Your ID: {message.ChatId}");
                    default:
                        return await api.SendMessage(message.ChatId, welcomeMessage);
                }
            }
            return "";
        }
    }
}
