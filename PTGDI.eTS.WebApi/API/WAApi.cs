﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using PTGDI.eTS.WebApi.Helpers;

namespace PTGDI.eTS.WebApi.API
{
    public class WAApi
    {
        private string APIUrl = "https://eu135.chat-api.com/instance139685/";
        private string token = "0xbu4x60y2nu2ktt";

        public WAApi(string aPIUrl, string token)
        {
            APIUrl = aPIUrl;
            this.token = token;
        }

        public async Task<string> SendRequest(string method, string data)
        {
            string url = $"{APIUrl}{method}?token={token}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                var result = await client.PostAsync("", content);
                return await result.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> SendMessage(string chatID, string text)
        {
            var data = new Dictionary<string, string>()
            {
                {"chatId",chatID },
                { "body", text }
            };
            return await SendRequest("sendMessage", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendOgg(string chatID, string ogg)
        {
            var data = new Dictionary<string, string>
            {
                {"audio", ogg },
                {"chatId", chatID }
            };

            return await SendRequest("sendAudio", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendGeo(string chatID, Double lat, Double lon, string caption)
        {
            var data = new Dictionary<string, string>()
            {
                { "lat", lat.ToString() },
                { "lng", lon.ToString() },
                { "address", caption },
                { "chatId", chatID}
            };
            return await SendRequest("sendLocation", JsonConvert.SerializeObject(data));

        }

        public async Task<string> CreateGroup(string author, string groupName, string messageText = "Welcome to this group!")
        {
            var phone = author.Replace("@c.us", "");
            var data = new Dictionary<string, string>()
            {
                { "groupName", groupName},
                { "phones", phone },
                { "messageText", messageText }
            };
            return await SendRequest("group", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendFile(string chatID, string path)
        {
            var data = new Dictionary<string, string>(){
                    { "chatId", chatID },
                    { "body", Converter.convertFileToBase64(path) },
                    { "filename", "yourfile" },
                    { "caption", $"Your file" }
                };

            return await SendRequest("sendFile", JsonConvert.SerializeObject(data));
        }
    }
}
