﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PTGDI.eTS.WebApi.Helpers
{
    public class Converter
    {
        public static string convertFileToBase64(string fullPathToImage)
        {
            Byte[] bytes = File.ReadAllBytes(fullPathToImage);
            String base64Encoded = Convert.ToBase64String(bytes);
            return base64Encoded;
        }
    }
}
